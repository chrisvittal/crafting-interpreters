module Language.Lox.Token where

data Token = Token TokenType String LoxData Int
  deriving Eq

simpleToken :: TokenType -> String -> Int -> Token
simpleToken tt s l = Token tt s LoxNone l

instance Show Token where
  show (Token typ lexe dat _) = show typ ++ " " ++ lexe ++ " " ++ show dat

data TokenType =
    LeftParenTT | RightParenTT | LeftBraceTT | RightBraceTT | CommaTT | DotTT | MinusTT
  | PlusTT | SemicolonTT | SlashTT | StarTT | BangTT | BangEqualTT | EqualTT | EqualEqualTT
  | GreaterTT | GreaterEqualTT | LessTT | LessEqualTT | IdentTT | StringTT | NumberTT
  | AndTT | ClassTT | ElseTT | FalseTT | FunTT | ForTT | IfTT | NilTT | OrTT | PrintTT
  | ReturnTT | SuperTT | ThisTT | TrueTT | VarTT | WhileTT | ErrorTT | EofTT
    deriving (Eq, Show)

data LoxData = LoxNone
  deriving (Eq, Show)
