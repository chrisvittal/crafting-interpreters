module Language.Lox (
  run,
) where

import Language.Lox.Token

data LoxError = LoxCompileErr | LoxRuntimeErr
  deriving (Eq, Show)

run :: String -> IO ()
run = printTokens . scan

scan :: String -> [Token]
scan = runScan 1

runScan :: Int -> String -> [Token]
runScan line [] = [Token EofTT "" LoxNone line]
runScan line s@(c:cs) = case c of
    '(' -> simpleToken LeftParenTT [c] line : runScan line cs
    ')' -> simpleToken RightParenTT [c] line : runScan line cs
    '{' -> simpleToken LeftBraceTT [c] line : runScan line cs
    '}' -> simpleToken RightBraceTT [c] line : runScan line cs
    ',' -> simpleToken CommaTT [c] line : runScan line cs
    '.' -> simpleToken DotTT [c] line : runScan line cs
    '-' -> simpleToken MinusTT [c] line : runScan line cs
    '+' -> simpleToken PlusTT [c] line : runScan line cs
    ';' -> simpleToken SemicolonTT [c] line : runScan line cs
    '/' -> simpleToken SlashTT [c] line : runScan line cs
    '*' -> simpleToken StarTT [c] line : runScan line cs
    '!' -> matchToken '!' '=' cs line BangEqualTT BangTT
    '=' -> matchToken '=' '=' cs line EqualEqualTT EqualTT
    '<' -> matchToken '<' '=' cs line LessEqualTT LessTT
    '>' -> matchToken '>' '=' cs line GreaterEqualTT GreaterTT
    _ -> Token ErrorTT "Unexpected character" LoxNone line : runScan line cs

matchToken :: Char -> Char -> String -> Int -> TokenType -> TokenType -> [Token]
matchToken c _ [] line _ no = simpleToken no [c] line : runScan line []
matchToken c e (h:ss) line yes no 
  | e == h    = simpleToken yes [c,h] line : runScan line ss
  | otherwise = simpleToken no [c] line : runScan line (h:ss)

printTokens :: [Token] -> IO ()
printTokens [] = return ()
printTokens [x] = print x
printTokens (x:xs) = case x of
  Token EofTT _ _ _ -> print x
  _ -> print x >> printTokens xs

