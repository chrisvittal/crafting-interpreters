{-# LANGUAGE CPP #-}
module Main where

import System.Environment
import System.Exit

import System.Console.Haskeline

import Language.Lox (run)

#include <sysexits.h>

main :: IO ()
main = getArgs >>= processArgs

processArgs :: [String] -> IO ()
processArgs [] = repl
processArgs [path] = readFile path >>= runFile
processArgs _ = putStrLn "Usage: hslox [path]" >> exitWith (ExitFailure EX_USAGE)

repl :: IO ()
repl = runInputT (setComplete noCompletion defaultSettings) loop
  where
    loop = do
      line <- getInputLine "λ lox> "
      case line of 
        Nothing -> return ()
        Just ln -> return (run ln) >> loop

runFile :: String -> IO ()
runFile = run
