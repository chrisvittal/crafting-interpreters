

BUILD_DIR := build
JAVA_DIR :=src/main/java
C_DIR := c

ifeq (4.2,$(firstword $(sort $(MAKE_VERSION) 4.2)))
res := $(shell python3 util/generate_ast $(JAVA_DIR)/com/craftinginterpreters/lox)
ifneq ($(.SHELLSTATUS), 0)
generation_fail:
	@ echo $(res)
	@ echo 'class generation failed, check output to see the error'
	@ exit 1
endif
else
res := $(shell python3 util/generate_ast $(JAVA_DIR)/com/craftinginterpreters/lox ; echo $$?)
ifneq ($(res), 0)
generation_fail:
	@ echo 'class generation failed, check output to see the error'
	@ exit 1
endif
endif

JAVA_FLAGS := -Werror
JAVA_PACKAGES := lox

get_java_srcs = $(wildcard $(addprefix $(2)/com/craftinginterpreters/, \
	$(addsuffix /*.java, $(1))))

JAVA_SRCS := $(call get_java_srcs, $(JAVA_PACKAGES), $(JAVA_DIR))
JAVA_CLASSES := $(patsubst $(JAVA_DIR)/%, $(BUILD_DIR)/java/%, \
	$(JAVA_SRCS:.java=.class))

CFLAGS += -Wall -Wextra -Wpedantic
LDFLAGS += -Wl,--no-undefined -Wl,--as-needed

C_SRCS := $(wildcard c/*.c)
OBJECTS := $(addprefix $(BUILD_DIR)/release_, $(C_SRCS:.c=.o))
DBGOBJECTS := $(addprefix $(BUILD_DIR)/debug_, $(C_SRCS:.c=.o))

.PHONY: all clean distclean printj debug default

default: jlox dclox

all: jlox dclox clox

release: jlox clox

printj:
	@ echo $(JAVA_SRCS)

printjc:
	@ echo $(JAVA_CLASSES)

jlox: $(JAVA_CLASSES) $(GENERATED_CLASSES)
	@ rm -f jlox && touch jlox
	@ echo '#!/bin/sh' > jlox
	@ echo 'java -cp $(BUILD_DIR)/java com.craftinginterpreters.lox.Lox $$@' >> jlox
	@ chmod +x jlox

$(BUILD_DIR)/java/%.class: $(JAVA_DIR)/%.java $(BUILD_DIR)/java
	javac -cp $(JAVA_DIR) -d $(BUILD_DIR)/java $(JAVA_FLAGS) -implicit:none $<

$(BUILD_DIR)/java:
	mkdir -p $@

clox: $(OBJECTS)
	@ $(CC) -O3 $(CFLAGS) -Werror -flto $^ -o $@ $(LDFLAGS)
	@ printf "%8s %-30s %s\n" $(CC) $@ "$(CFLAGS) -O3 -flto $(LDFLAGS)"

dclox: $(DBGOBJECTS)
	@ $(CC) -O0 $(CFLAGS) -g $^ -o $@ $(LDFLAGS)
	@ printf "%8s %-30s %s\n" $(CC) $@ "$(CFLAGS) -O0 -g $(LDFLAGS)"

$(BUILD_DIR)/debug_c/%.o: $(C_DIR)/%.c
	@ mkdir -p $(dir $@)
	@ $(CC) -O0 $(CFLAGS) -g $< -c -o $@ -MD -MQ $@ -MF \
		$(addsuffix .d, $@)
	@ printf "%8s %-30s %s\n" $(CC) $< "$(CFLAGS) -O0 -g"


$(BUILD_DIR)/release_c/%.o: $(C_DIR)/%.c
	@ mkdir -p $(dir $@)
	@ $(CC) -O3 $(CFLAGS) -Werror -flto $< -c -o $@ -MD -MQ $@ -MF \
		$(addsuffix .d, $@)
	@ printf "%8s %-30s %s\n" $(CC) $< "$(CFLAGS) -O3 -flto"

clean: 
	rm -rf $(BUILD_DIR)

distclean: clean
	rm -f jlox clox dclox

-include $(BUILD_DIR)/release_c/*.d $(BUILD_DIR)/debug_c/*.d
