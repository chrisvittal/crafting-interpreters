
#include <stdio.h>

#include "memory.h"
#include "value.h"

void print_value(Value val)
{
	printf("%g", val);
}

void init_value_array(ValueArray *va)
{
	va->len = 0;
	va->cap = 0;
	va->ptr = NULL;
}

void write_value_array(ValueArray *va, Value val)
{
	if (va->len == va->cap) {
		uint32_t new_cap = GROW_CAP(va->cap);
		Value *new_ptr = GROW_ARRAY(va->ptr, Value, va->cap, new_cap);
		va->cap = new_cap;
		va->ptr = new_ptr;
	}

	va->ptr[va->len] = val;
	va->len++;
}

void free_value_array(ValueArray *va)
{
	FREE_ARRAY(Value, va->ptr, va->cap);
	init_value_array(va);
}
