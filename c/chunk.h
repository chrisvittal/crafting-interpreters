#ifndef CLOX_CHUNK_H
#define CLOX_CHUNK_H

#include "common.h"
#include "line_array.h"
#include "value.h"

typedef enum {
	OP_CONSTANT,
	OP_CONSTANT_LONG,
	OP_ADD,
	OP_SUBTRACT,
	OP_MULTIPLY,
	OP_DIVIDE,
	OP_NEGATE,
	OP_RETURN,
} OpCode;

typedef struct {
	uint32_t len;
	uint32_t cap;
	uint8_t *ptr;
	ValueArray constants;
	LineArray lines;
} Chunk;

void init_chunk(Chunk *chunk);
void free_chunk(Chunk *chunk);
void write_chunk(Chunk *chunk, uint8_t byte, uint32_t line);
uint32_t add_constant(Chunk *chunk, Value value);
void write_constant(Chunk *chunk, Value value, uint32_t line);

uint32_t get_line(Chunk *chunk, uint32_t index);

#endif
/* vim: set ft=c: */
