#include <limits.h>
#include <stdlib.h>

#include "chunk.h"
#include "memory.h"

void init_chunk(Chunk *chunk)
{
	chunk->len = 0;
	chunk->cap = 0;
	chunk->ptr = NULL;
	init_line_array(&chunk->lines);
	init_value_array(&chunk->constants);
}

void free_chunk(Chunk *chunk)
{
	FREE_ARRAY(uint8_t, chunk->ptr, chunk->cap);
	free_value_array(&chunk->constants);
	init_chunk(chunk);
}

void write_chunk(Chunk *chunk, uint8_t byte, uint32_t line)
{
	if (chunk->len == chunk->cap) {
		uint32_t new_cap = GROW_CAP(chunk->cap);
		uint8_t *new_ptr = GROW_ARRAY(chunk->ptr, uint8_t, chunk->cap, new_cap);
		chunk->cap = new_cap;
		chunk->ptr = new_ptr;
	}
	int32_t line_offset = line - chunk->lines.prev_line;
	chunk->lines.prev_line = line;
	if (chunk->lines.first_line == 0) {
		chunk->lines.first_line = line;
		line_offset = 0;
	}
	write_line_array(&chunk->lines, line_offset);
	chunk->ptr[chunk->len] = byte;
	chunk->len++;
}

uint32_t add_constant(Chunk *chunk, Value value)
{
	write_value_array(&chunk->constants, value);
	return chunk->constants.len - 1;
}

void write_constant(Chunk *chunk, Value value, uint32_t line)
{
	uint32_t index = add_constant(chunk, value);
	if (index < UCHAR_MAX) {
		write_chunk(chunk, OP_CONSTANT, line);
		write_chunk(chunk, index, line);
	} else { /* OP_CONSTANT_LONG */
		write_chunk(chunk, OP_CONSTANT_LONG, line);
		/* write little endian */
		write_chunk(chunk, index & 0xFF, line);
		write_chunk(chunk, (index >> 8) & 0xFF, line);
		write_chunk(chunk, (index >> 16) & 0xFF, line);
	}
}

/* the third parameter is a pointer to a structure used to memoize where the iteration of
 * this function ended up */
uint32_t get_line(Chunk *chunk, uint32_t index)
{
	LineArray la = chunk->lines;
	uint32_t addr = 0, line = la.first_line;
	for (struct LineEntry *le = la.ptr; le < la.ptr+la.len; le++) {
		addr += le->byte_offset;
		if (addr > index)
			break;
		line += le->line_offset;
	}
	return line;
}
