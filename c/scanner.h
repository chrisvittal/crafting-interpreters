#ifndef CLOX_SCANNER_H
#define CLOX_SCANNER_H

#include "common.h"

typedef enum {
	/* single characters */
	TOKEN_LEFT_PAREN, TOKEN_RIGHT_PAREN, TOKEN_LEFT_BRACE, TOKEN_RIGHT_BRACE,
	TOKEN_COMMA, TOKEN_DOT, TOKEN_MINUS, TOKEN_PLUS, TOKEN_SEMICOLON,
	TOKEN_SLASH, TOKEN_STAR,

	/* one or two characters */
	TOKEN_BANG, TOKEN_BANG_EQUAL, TOKEN_EQUAL, TOKEN_EQUAL_EQUAL,
	TOKEN_GREATER, TOKEN_GREATER_EQUAL, TOKEN_LESS, TOKEN_LESS_EQUAL,

	/* literals */
	TOKEN_IDENTIFIER, TOKEN_STRING, TOKEN_NUMBER,

	/* keywords */
	TOKEN_AND, TOKEN_CLASS, TOKEN_ELSE, TOKEN_FALSE, TOKEN_FUN, TOKEN_FOR,
	TOKEN_IF, TOKEN_NIL, TOKEN_OR, TOKEN_PRINT, TOKEN_RETURN, TOKEN_SUPER,
	TOKEN_THIS, TOKEN_TRUE, TOKEN_VAR, TOKEN_WHILE,

	/* meta tokens */
	TOKEN_ERROR, TOKEN_EOF,
} TokenType;

typedef struct {
	const char *start;
	const char *current;
	uint32_t line;
} Scanner;

typedef struct {
	TokenType type;
	const char *start;
	uint32_t length;
	uint32_t line;
} Token;

void init_scanner(Scanner *scanner, const char *source);
Token scan_token(Scanner *scanner);

#endif
/* vim: set ft=c: */
