
#include <stdio.h>

#include "compiler.h"
#include "debug.h"
#include "vm.h"

#define DEBUG_TRACE_EXECUTION

void init_vm(VM *vm)
{
	vm->sp = vm->stack;
}

void free_vm(VM *vm)
{
	(void)vm; // satisfy unused variable
}

void push(VM *vm, Value val)
{
	*vm->sp = val;
	vm->sp++;
}

Value pop(VM *vm)
{
	vm->sp--;
	return *vm->sp;
}

static InterpretResult run(VM *vm)
{
#define READ_BYTE() (*vm->ip++)
#define READ_CONST() (vm->chunk->constants.ptr[READ_BYTE()])
#define BINARY_OP(op) do { \
	Value __b = pop(vm); \
	Value __a = pop(vm); \
	push(vm, __a op __b); \
	} while(0)
#ifdef DEBUG_TRACE_EXECUTION
	uint32_t dbg_line = 0;
#endif
	for (;;) {
#ifdef DEBUG_TRACE_EXECUTION
		printf("        ");
		for (Value *s = vm->stack; s < vm->sp; s++) {
			printf("[ ");
			print_value(*s);
			printf(" ]");
		}
		printf("\n");
		disassemble_instruction(vm->chunk,
								(uint32_t)(vm->ip - vm->chunk->ptr),
								&dbg_line);
#endif
		uint8_t instruction;
		switch (instruction = READ_BYTE()) {
		case OP_RETURN:
			print_value(pop(vm));
			printf("\n");
			return INTERPRET_OK;
		case OP_CONSTANT: {
			Value constant = READ_CONST();
			print_value(constant); putchar('\n');
			push(vm, constant);
			break;
		}
		case OP_CONSTANT_LONG: {
			Value constant = vm->chunk->constants.ptr[
				vm->ip[0] + (vm->ip[1] >> 8) + (vm->ip[2] >> 16)];
			vm->ip += 3;
			push(vm, constant);
			break;
		}
		case OP_ADD: BINARY_OP(+); break;
		case OP_SUBTRACT: BINARY_OP(-); break;
		case OP_MULTIPLY: BINARY_OP(*); break;
		case OP_DIVIDE: BINARY_OP(/); break;
		case OP_NEGATE: push(vm, -pop(vm)); break;
		}
	}
#undef READ_BYTE
#undef READ_CONST
}

InterpretResult interpret(VM *vm, const char *source)
{
	compile(source);
	return INTERPRET_OK;
}
