#include <stdio.h>
#include <string.h>

#include "scanner.h"

void init_scanner(Scanner *scanner, const char *source)
{
	scanner->start = source;
	scanner->current = source;
	scanner->line = 1;
}

static char advance(Scanner *s)
{
	s->current++;
	return s->current[-1];
}

static char peek(Scanner *s)
{
	return *s->current;
}

static bool at_end(Scanner *s)
{
	return *s->current == '\0';
}

static char peek_next(Scanner *s)
{
	if (at_end(s)) return '\0';
	return s->current[1];
}

static bool is_digit(char c)
{
	return c >= '0' && c <= '9';
}

static bool is_alpha(char c)
{
	return (c >= 'a' && c <= 'z')
		|| (c >= 'A' && c <= 'Z')
		|| c == '_';
}

static bool match(Scanner *s, char c)
{
	if (at_end(s)) return false;
	if (*s->current != c) return false;

	s->current++;
	return true;
}

static void skip_whitespace(Scanner *s)
{
	for (;;) {
		char c = peek(s);
		switch (c) {
		case ' ':
		case '\t':
		case '\r':
			advance(s);
			break;
		case '\n':
			s->line++;
			advance(s);
			break;
		/* TODO(cdv): add block comments */
		case '/':
			if (peek_next(s) == '/') {
				while (peek(s) != '\n' && !at_end(s)) advance(s);
			} else {
				return;
			}
			break;
		}
	}
}

static Token make_token(Scanner *s, TokenType type)
{
	Token token = {
		.type = type,
		.start = s->start,
		.length = s->current - s->start,
		.line = s->line
	};
	return token;
}

static Token error_token(Scanner *s, const char *message)
{
	Token token = {
		.type = TOKEN_ERROR,
		.start = message,
		.length = strlen(message),
		.line = s->line,
	};
	return token;
}

static Token string(Scanner *s)
{
	while (peek(s) != '"' && !at_end(s)) {
		if (peek(s) == '\n') s->line++;
		advance(s);
	}

	if (at_end(s)) return error_token(s, "Unterminated string");

	advance(s);
	return make_token(s, TOKEN_STRING);
}

static Token number(Scanner *s)
{
	while (is_digit(peek(s))) advance(s);

	if (peek(s) == '.' && is_digit(peek_next(s))) {
		advance(s);

		while (is_digit(peek(s))) advance(s);
	}
	return make_token(s, TOKEN_NUMBER);
}

static TokenType keyword(Scanner *s,
                         uint32_t start,
                         uint32_t len,
                         const char *rest,
                         TokenType type)
{
	if (s->current - s->start == start + len
		&& memcmp(s->start + start, rest, len) == 0) {
		return type;
	}
	return TOKEN_IDENTIFIER;
}

static Token identifier(Scanner *s)
{
	while (is_alpha(peek(s)) || is_digit(peek(s))) advance(s);

	TokenType type = TOKEN_IDENTIFIER;
	switch (s->start[0]) {
	case 'a': type = keyword(s, 1, 2, "nd", TOKEN_AND); break;
	case 'c': type = keyword(s, 1, 4, "lass", TOKEN_CLASS); break;
	case 'e': type = keyword(s, 1, 3, "lse", TOKEN_ELSE); break;
	case 'f':
		if (s->current - s->start > 1) {
			switch (s->start[1]) {
			case 'a': type = keyword(s, 2, 3, "lse", TOKEN_FALSE); break;
			case 'o': type = keyword(s, 2, 1, "r", TOKEN_FOR); break;
			case 'u': type = keyword(s, 2, 1, "n", TOKEN_FUN); break;
			}
		}
		break;
	case 'i': type = keyword(s, 1, 1, "f", TOKEN_IF); break;
	case 'n': type = keyword(s, 1, 2, "il", TOKEN_NIL); break;
	case 'o': type = keyword(s, 1, 1, "r", TOKEN_OR); break;
	case 'p': type = keyword(s, 1, 4, "rint", TOKEN_PRINT); break;
	case 'r': type = keyword(s, 1, 5, "eturn", TOKEN_RETURN); break;
	case 's': type = keyword(s, 1, 4, "uper", TOKEN_SUPER); break;
	case 't':
		if (s->current - s->start > 1) {
			switch (s->start[1]) {
			case 'h': type = keyword(s, 2, 2, "is", TOKEN_THIS); break;
			case 'r': type = keyword(s, 2, 2, "ue", TOKEN_TRUE); break;
			}
		}
		break;
	case 'v': type = keyword(s, 1, 2, "ar", TOKEN_VAR); break;
	case 'w': type = keyword(s, 1, 4, "hile", TOKEN_WHILE); break;
	}
	return make_token(s, type);
}

Token scan_token(Scanner *scn)
{
	skip_whitespace(scn);
	scn->start = scn->current;
	if (at_end(scn)) return make_token(scn, TOKEN_EOF);

	char c = advance(scn);

	if (is_alpha(c)) return identifier(scn);
	if (is_digit(c)) return number(scn);

	switch (c) {
	case '(': return make_token(scn, TOKEN_LEFT_PAREN);
	case ')': return make_token(scn, TOKEN_RIGHT_PAREN);
	case '{': return make_token(scn, TOKEN_LEFT_BRACE);
	case '}': return make_token(scn, TOKEN_RIGHT_BRACE);
	case ',': return make_token(scn, TOKEN_COMMA);
	case '.': return make_token(scn, TOKEN_DOT);
	case '-': return make_token(scn, TOKEN_MINUS);
	case '+': return make_token(scn, TOKEN_PLUS);
	case ';': return make_token(scn, TOKEN_SEMICOLON);
	case '/': return make_token(scn, TOKEN_SLASH);
	case '*': return make_token(scn, TOKEN_STAR);
	case '!': return make_token(scn, match(scn, '=') ? TOKEN_BANG_EQUAL : TOKEN_BANG);
	case '=': return make_token(scn, match(scn, '=') ? TOKEN_EQUAL_EQUAL : TOKEN_EQUAL);
	case '<': return make_token(scn, match(scn, '=') ? TOKEN_LESS_EQUAL : TOKEN_LESS);
	case '>': return make_token(scn, match(scn, '=') ? TOKEN_GREATER_EQUAL : TOKEN_GREATER);
	case '"': return string(scn);
	}

	return error_token(scn, "Unexpected character");
}
