#ifndef CLOX_MEMORY_H
#define CLOX_MEMORY_H

#include <stddef.h>

#define GROW_CAP(cap) \
	((cap) < 8 ? 8 : (cap) * 2)

#define GROW_ARRAY(prev, type, old_cap, new_cap) \
	(type*)reallocate(prev, sizeof(type) * (old_cap), sizeof(type) * (new_cap))

#define FREE_ARRAY(type, ptr, old_cap) \
	(type*)reallocate(ptr, sizeof(type) * (old_cap), 0)

void *reallocate(void *prev, size_t old_size, size_t new_size);

#endif
/* vim: set ft=c: */
