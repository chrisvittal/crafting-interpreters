#include <stdio.h>

#include "common.h"
#include "compiler.h"
#include "scanner.h"

void compile(const char *source)
{
	Scanner scanner;
	init_scanner(&scanner, source);
	uint32_t line = -1;

	for (;;) {
		Token token = scan_token(&scanner);
		if (token.line != line) {
			printf("%4d ", line);
			line = token.line;
		} else {
			printf("   | ");
		}

		printf("%2d '%.*s'\n", token.type, token.length, token.start);

		if (token.type == TOKEN_EOF) break;
	}
}
