#include <stdlib.h>

#include "common.h"
#include "memory.h"

void *reallocate(void *prev, size_t old_size, size_t new_size)
{
	(void)old_size;
	return realloc(prev, new_size);
}
