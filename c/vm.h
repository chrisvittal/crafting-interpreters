#ifndef CLOX_VM_H
#define CLOX_VM_H

#include "chunk.h"
#include "value.h"

#define STACK_MAX 256

typedef struct {
    Chunk *chunk;
    uint8_t *ip; /* instruction pointer */
    Value stack[STACK_MAX];
    Value *sp; /* stack pointer */
} VM;

typedef enum {
	INTERPRET_OK,
	INTERPRET_COMPILE_ERROR,
	INTERPRET_RUNTIME_ERROR,
} InterpretResult;

void init_vm(VM *vm);
void free_vm(VM *vm);
InterpretResult interpret(VM *vm, const char *source);
void push(VM *vm, Value val);
Value pop(VM *vm);

#endif
/* vim: set ft=c: */
