#ifndef CLOX_DEBUG_H
#define CLOX_DEBUG_H

#include "chunk.h"

void disassemble_chunk(Chunk *chunk, const char *name);
uint32_t disassemble_instruction(Chunk *chunk, uint32_t i, uint32_t *prev);
void dump_chunk(Chunk *chunk, const char *name);

#endif
/* vim: set ft=c: */
