#ifndef CLOX_VALUE_H
#define CLOX_VALUE_H

#include "common.h"

typedef double Value;

void print_value(Value val);

typedef struct {
	uint32_t len;
	uint32_t cap;
	Value *ptr;
} ValueArray;

void init_value_array(ValueArray *va);
void write_value_array(ValueArray *va, Value val);
void free_value_array(ValueArray *va);

#endif
/* vim: set ft=c: */
