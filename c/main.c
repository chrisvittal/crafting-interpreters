#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>

#include "common.h"
#include "chunk.h"
#include "debug.h"
#include "vm.h"

#define INIT_LINE_SIZ 1024

static long read_line(char **line, size_t *linesiz, FILE *file)
{
	if (line == NULL || linesiz == NULL) {
		return -EX_SOFTWARE;
	}
	if (*line == NULL) {
		*line = malloc(INIT_LINE_SIZ+1);
		*linesiz = INIT_LINE_SIZ;
		if (*line == NULL) {
			perror("malloc");
			return -EX_IOERR;
		}
	}
	size_t linelen = 0;
	int ch;

	while ((ch = fgetc(file)) != EOF) {
		if (linelen == *linesiz) {
			char *tmp = realloc(*line, *linesiz*2+1);
			if (tmp == NULL) {
				perror("malloc");
				free(line);
				return -EX_IOERR;
			}
			*linesiz *= 2;
			*line = tmp;
		}

		(*line)[linelen] = (char)ch;
		linelen++;
		if (ch == '\n')
			break;
	}
	(*line)[linelen] = '\0';

	return linelen;
}

static int repl(VM *vm)
{
	char *line = NULL;
	size_t linesiz = 0;

	for (;;) {
		printf("lox> ");

		long read = read_line(&line, &linesiz, stdin);
		if (read <= 0) {
			putchar('\n');
			free(line);
			return -read;
		}

		interpret(vm, line);
	}
}

static char *read_file(const char* path)
{
	char *buffer;
	FILE *file = fopen(path, "rb");
	if (file == NULL) {
		fprintf(stderr, "could not open file \"%s\".\n", path);
		fprintf(stderr, "os error: %s\n", strerror(errno));
		return NULL;
	}

	if (fseek(file, 0, SEEK_END) == -1) {
		perror("fseek");
		buffer = NULL;
		goto rf_exit;
	}
	size_t fsiz = ftell(file);
	if (fsiz == (size_t)-1) {
		perror("ftell");
		return NULL;
	}
	rewind(file);

	buffer = malloc(fsiz + 1);
	if (buffer == NULL) {
		perror("malloc");
		goto rf_exit;
	}

	size_t read = fread(buffer, sizeof(char), fsiz, file);
	if (read < fsiz) {
		fprintf(stderr, "could not read file \"%s\".\n", path);
		free(buffer);
		buffer = NULL;
	}
	
rf_exit:
	fclose(file);
	return buffer;
}


static int run_file(VM *vm, const char* path)
{
	char *source = read_file(path);
	if (source == NULL) return EX_IOERR;
	InterpretResult result = interpret(vm, source);
	free(source);
	
	switch (result) {
	case INTERPRET_COMPILE_ERROR: return EX_DATAERR;
	case INTERPRET_RUNTIME_ERROR: return EX_SOFTWARE;
	default: return EX_OK;
	}
}

int main(int argc, const char *argv[])
{
	int ex;
	VM vm;
	init_vm(&vm);

	if (argc == 1) {
		ex = repl(&vm);
	} else if (argc == 2) {
		ex = run_file(&vm, argv[1]);
	} else {
		fprintf(stderr, "Usage: clox [path]\n");
		ex = EX_USAGE;
	}

	free_vm(&vm);
	exit(ex);
}
