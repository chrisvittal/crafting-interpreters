#ifndef CLOX_LINE_ARRAY_H
#define CLOX_LINE_ARRAY_H

#include "common.h"

struct LineEntry {
	uint8_t byte_offset;
	int8_t line_offset;
};

typedef struct {
	uint32_t len;
	uint32_t cap;
	uint32_t prev_line;
	uint32_t first_line;
	uint32_t next_byte_off;
	struct LineEntry *ptr;
} LineArray;

void init_line_array(LineArray *la);
void free_line_array(LineArray *la);

/* only writes a new entry to the array if the line is different or
 * the length is UINT32_MAX */
void write_line_array(LineArray *la, int32_t line_offset);

#endif
/* vim: set ft=c: */
