
#include <stdio.h>

#include "chunk.h"
#include "debug.h"
#include "value.h"

void disassemble_chunk(Chunk *chunk, const char *name)
{
	printf("== %s ==\n", name);

	uint32_t prev_line = 0;
	for (uint32_t i = 0; i < chunk->len; ) {
		i = disassemble_instruction(chunk, i, &prev_line);
	}
}

static uint32_t constant_instruction(const char *name, Chunk *chunk, uint32_t offset)
{
	uint8_t constant = chunk->ptr[offset + 1];
	printf("%-16s %4d '", name, constant);
	print_value(chunk->constants.ptr[constant]);
	printf("'\n");
	return offset + 2;
}

static uint32_t constant_long_instruction(const char *name, Chunk *chunk, uint32_t offset)
{
	uint8_t *p = chunk->ptr+offset;
	uint32_t constant = (uint32_t)p[1] + ((uint32_t)p[2] >> 8) + ((uint32_t)p[3] >> 16);
	printf("%-16s %4d '", name, constant);
	print_value(chunk->constants.ptr[constant]);
	printf("'\n");
	return offset + 4;
}

static uint32_t simple_instruction(const char *name, uint32_t offset)
{
	printf("%s\n", name);
	return offset + 1;
}

uint32_t disassemble_instruction(Chunk *chunk, uint32_t offset, uint32_t *prev_line)
{
	printf("%04d ", offset);
	uint8_t inst = chunk->ptr[offset];
	uint32_t line = get_line(chunk, offset);
	if (offset > 0 && prev_line != NULL && *prev_line == line) {
		printf("   | ");
	} else {
		printf("%4d ", line);
	}
	if (prev_line != NULL)
		*prev_line = line;
	switch (inst) {
	case OP_CONSTANT:
		return constant_instruction("OP_CONSTANT", chunk, offset);
	case OP_CONSTANT_LONG:
		return constant_long_instruction("OP_CONSTANT_LONG", chunk, offset);
	case OP_ADD: return simple_instruction("OP_ADD", offset);
	case OP_SUBTRACT: return simple_instruction("OP_SUBTRACT", offset);
	case OP_MULTIPLY: return simple_instruction("OP_MULTIPLY", offset);
	case OP_DIVIDE: return simple_instruction("OP_DIVIDE", offset);
	case OP_NEGATE: return simple_instruction("OP_NEGATE", offset);
	case OP_RETURN: return simple_instruction("OP_RETURN", offset);
	default:
		printf("Unknown opcode %d\n", offset);
		return offset + 1;
	}
}

void dump_chunk(Chunk *chunk, const char *name)
{
	printf("== %s ==\n", name);
	printf("=== chunk array ===\n");
	for (uint32_t i = 0; i < chunk->len; i++) {
		if (i % 16 == 0 && i != 0)
			printf("\n");
		if (i % 16 == 0)
			printf("%04x:", i);
		printf("  %02x", chunk->ptr[i]);
	}
	printf("\n=== line array ===\n");
	for (uint32_t i = 0; i < chunk->lines.len; i++) {
		if (i % 4 == 0 && i != 0)
			printf("\n");
		if (i % 4 == 0)
			printf("%04x:", i);
		struct LineEntry v = chunk->lines.ptr[i];
		printf("  (b: %02hhx, l: %02hhx)", v.byte_offset, v.line_offset);
	}
	printf("\n");
}
