
#include <assert.h>
#include <limits.h>

#include "memory.h"
#include "line_array.h"

void init_line_array(LineArray *la)
{
	la->len = 0;
	la->cap = 0;
	la->prev_line = 0;
	la->first_line = 0;
	la->next_byte_off = -1; /* first byte offset needs to be one less
	                           than others if they're calculated in the
	                           same way*/
	la->ptr = NULL;
}

void free_line_array(LineArray *la)
{
	FREE_ARRAY(struct LineEntry, la->ptr, la->cap);
	init_line_array(la);
}

void write_line_array(LineArray *la, int32_t line_offset)
{
	/* if this function is called, we're always increasing the next byte
	   we have to write */
	la->next_byte_off++;
	if (line_offset == 0) {
		return;
	}
	while (line_offset != 0) {
		if (la->len == la->cap) {
			uint32_t new_cap = GROW_CAP(la->cap);
			struct LineEntry *new_ptr = GROW_ARRAY(la->ptr, struct LineEntry,
			                                       la->cap, new_cap);
			la->cap = new_cap;
			la->ptr = new_ptr;
		}

		if (la->next_byte_off > UINT8_MAX) {
			la->ptr[la->len].byte_offset = UINT8_MAX;
			la->ptr[la->len].line_offset = 0;
			la->next_byte_off -= UINT8_MAX;
		} else {
			la->ptr[la->len].byte_offset = la->next_byte_off;
			la->next_byte_off = 0;
			if (line_offset < INT8_MIN) {
				la->ptr[la->len].line_offset = INT8_MIN;
				line_offset -= INT8_MIN;
			} else if (line_offset > INT8_MAX) {
				la->ptr[la->len].line_offset = INT8_MAX;
				line_offset -= INT8_MAX;
			} else {
				la->ptr[la->len].line_offset = line_offset;
				line_offset = 0; /* break the loop */
			}
		}
		la->len++;
	}
}
