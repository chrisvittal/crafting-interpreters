use std::fmt;

#[derive(PartialEq, Eq, Debug, Clone)]
pub enum LoxObject {
    Str(Box<str>),
    #[doc(hidden)]
    __Nonexhaustive
}

impl From<String> for LoxObject {
    fn from(it: String) -> Self {
        LoxObject::Str(it.into())
    }
}

impl From<Box<str>> for LoxObject {
    fn from(it: Box<str>) -> Self {
        LoxObject::Str(it)
    }
}

impl fmt::Display for LoxObject {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::LoxObject::*;
        match self {
            Str(ref s) => write!(f, "{}", s),
            _ => panic!()
        }
    }
}
