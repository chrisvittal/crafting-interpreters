
use std::cmp::Ordering;
use std::fmt;
use std::ops;
use std::rc::Rc;

use object::LoxObject;

#[derive(PartialEq,Debug,Clone)]
pub enum Value {
    Bool(bool),
    Num(f64),
    Obj(Rc<LoxObject>),
    Nil,
}

impl From<LoxObject> for Value {
    fn from(it: LoxObject) -> Self {
        Value::Obj(Rc::new(it))
    }
}

impl From<bool> for Value {
    #[inline]
    fn from(other: bool) -> Self {
        Value::Bool(other)
    }
}

impl ops::Add for Value {
    type Output = Option<Self>;
    fn add(self, rhs: Self) -> Self::Output {
        use Value::*;
        use self::LoxObject::*;
        match (self, rhs) {
            (Num(l), Num(r)) => Some(Num(l+r)),
            (Obj(l), Obj(r)) => match (l.as_ref(), r.as_ref()) {
                (Str(a), Str(b)) => Some(LoxObject::from(format!("{}{}", a, b)).into()),
                _ => None,
            }
            _ => None,
        }
    }
}

impl ops::Div for Value {
    type Output = Option<Self>;
    fn div(self, rhs: Self) -> Self::Output {
        use Value::*;
        match (self, rhs) {
            (Num(l), Num(r)) => Some(Num(l/r)),
            _ => None,
        }
    }
}

impl ops::Mul for Value {
    type Output = Option<Self>;
    fn mul(self, rhs: Self) -> Self::Output {
        use Value::*;
        match (self, rhs) {
            (Num(l), Num(r)) => Some(Num(l*r)),
            _ => None,
        }
    }
}

impl ops::Sub for Value {
    type Output = Option<Self>;
    fn sub(self, rhs: Self) -> Self::Output {
        use Value::*;
        match (self, rhs) {
            (Num(l), Num(r)) => Some(Num(l-r)),
            _ => None,
        }
    }
}

impl ops::Neg for Value {
    type Output = Option<Self>;

    fn neg(self) -> Self::Output {
        match self {
            Value::Num(d) => Some(Value::Num(-d)),
            _ => None
        }
    }
}

impl ops::Not for Value {
    type Output = Self;
    fn not(self) -> Self {
        match self {
            Value::Nil | Value::Bool(false) => Value::Bool(true),
            _ => Value::Bool(false)
        }
    }
}

impl PartialOrd for Value {
    fn partial_cmp(&self, rhs: &Self) -> Option<Ordering> {
        use Value::*;
        match (self, rhs) {
            (Num(l), Num(r)) => l.partial_cmp(r),
            _ => None
        }
    }
}

impl fmt::Display for Value {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use Value::*;
        match self {
            Bool(p) => write!(f, "{}", p),
            Num(d) => write!(f, "{}", d),
            Obj(obj) => write!(f, "{}", obj),
            Nil => write!(f, "nil"),
        }
    }
}
