
extern crate exitcode;
extern crate lox;

use std::env;
use std::fs;
use std::io;
use std::io::prelude::*;

use lox::*;

use error::*;

mod error;

fn main() {

    let mut args = env::args();
    ::std::process::exit(match if args.len() == 1 {
        repl()
    } else if args.len() == 2 {
        run_file(&args.nth(1).unwrap())
    } else {
        eprintln!("Usage: rlox [path]");
        ::std::process::exit(exitcode::USAGE);
    } {
        Ok(_) => exitcode::OK,
        Err(LoxError::Io(e)) => {
            eprintln!("io error: {}", e);
            exitcode::IOERR
        }
        Err(LoxError::Lox(InterpreterError::Compile)) => {
            exitcode::DATAERR
        }
        Err(LoxError::Lox(InterpreterError::Runtime)) => {
            exitcode::SOFTWARE
        }
    })
}

fn repl() -> LoxResult {
    let mut line = String::new();
    let mut stdout = io::stdout();
    let mut vm = LoxVm::new();

    loop {
        stdout.write_all("rlox> ".as_ref())?;
        stdout.flush()?;
        io::stdin().read_line(&mut line)?;
        if line.is_empty() { println!(); return Ok(()); } // check if we recived eof
        let _ = vm.interpret(line.trim());
        line.clear();
    }
}

fn run_file(path: &str) -> LoxResult {
    let source = fs::read_to_string(path)?;
    Ok(LoxVm::new().interpret(&source)?)
}
