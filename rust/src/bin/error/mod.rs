
use std::io;
use std::fmt;

use lox::*;

pub enum LoxError {
    Io(io::Error),
    Lox(InterpreterError),
}

impl fmt::Display for LoxError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            LoxError::Io(ref e) => fmt::Display::fmt(e, f),
            LoxError::Lox(ref e) => fmt::Display::fmt(e, f),
        }
    }
}

pub type LoxResult = Result<(), LoxError>;

impl From<io::Error> for LoxError {
    fn from(e: io::Error) -> Self {
        LoxError::Io(e)
    }
}

impl From<InterpreterError> for LoxError {
    fn from(e: InterpreterError) -> Self {
        LoxError::Lox(e)
    }
}
