use std::cell::Cell;

use super::{Scanner, Token, TokenType};

pub(crate) struct Parser<'a> {
    pub(crate) scanner: Scanner<'a>,
    pub(crate) current: Token<'a>,
    pub(crate) prev: Token<'a>,
    pub(crate) has_error: Cell<bool>,
    pub(crate) panic_mode: Cell<bool>,
}

impl<'a> Parser <'a> {
    pub fn new(scanner: Scanner<'a>) -> Parser<'a> {
        Parser {
            scanner,
            current: Token::new(TokenType::Error, "No tokens read yet", 0),
            prev: Token::new(TokenType::Error, "No tokens read yet", 0),
            has_error: Cell::new(false),
            panic_mode: Cell::new(false)
        }
    }

    pub fn advance(&mut self) {
        ::std::mem::swap(&mut self.prev, &mut self.current);
        loop {
            let tok = self.scanner.next();
            match tok {
                Some(t@Token { typ: TokenType::Error, .. }) => {
                    self.current = t;
                    let msg = self.current.span;
                    self.error_at_cur(msg);
                }
                Some(t) => {
                    self.current = t;
                    break;
                }
                None => unreachable!("got none from scanner iterator"),
            }
        }
    }

    pub fn consume(&mut self, ttyp: TokenType, msg: &str) {
        if self.current.typ == ttyp {
            self.advance();
        } else {
            self.error_at_cur(msg);
        }
    }

    #[inline(always)]
    pub fn error_at_cur(&self, msg: &str) {
        self.error_at(&self.current, msg);
    }

    #[inline(always)]
    pub fn error(&self, msg: &str) {
        self.error_at(&self.prev, msg);
    }

    pub fn error_at(&self, tok: &Token, msg: &str) {
        if self.panic_mode.get() { return; }
        self.panic_mode.set(true);
        eprint!("[line {}] Error", tok.line);

        if tok.typ == TokenType::Eof {
            eprint!(" at end");
        } else if tok.typ == TokenType::Error {
            // do nothing
        } else {
            eprint!(" at '{}'", tok.span);
        }

        self.has_error.set(true);
        eprint!(": {}\n", msg);
    }
}
