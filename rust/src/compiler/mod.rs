
use std::ops::{Deref, DerefMut};
use std::rc::Rc;

use chunk::Chunk;
use {OpCode, InterpreterError};
use object::LoxObject;

mod parser;
mod scanner;

use self::parser::Parser;
use self::scanner::{Scanner, TokenType, Token};
use value::Value;

pub fn compile(source: &str) -> Result<Chunk, InterpreterError> {
    let scanner = Scanner::new(source);
    let parser = Parser::new(scanner);
    Compiler::new(parser).compile()
}

struct Compiler<'a> {
    parser: Parser<'a>,
    chunk: Chunk,
}

impl<'a> Compiler<'a> {
    fn new(parser: Parser<'a>) -> Self {
        Self {
            parser,
            chunk: Chunk::new(),
        }
    }

    fn compile(mut self) -> Result<Chunk, InterpreterError> {
        self.advance();
        self.expression();
        self.consume(TokenType::Eof, "Expected end of expression");
        self.emit_return(); // finish up
        #[cfg(debug_assertions)]
        {
            if !self.has_error.get() {
                self.chunk.disassemble("code")
            }
        }
        if self.has_error.get() {
            Err(InterpreterError::Compile)
        } else {
            Ok(self.chunk)
        }
    }

    fn binary(&mut self) {
        let op_typ = self.prev.typ;
        let rule = op_typ.get_rule();
        self.precedence(rule.precedence.up());

        match op_typ {
            TokenType::BangEqual => self.emit_opcodes(OpCode::Equal, OpCode::Not),
            TokenType::EqualEqual => self.emit_opcode(OpCode::Equal),
            TokenType::Greater => self.emit_opcode(OpCode::Greater),
            TokenType::GreaterEqual => self.emit_opcodes(OpCode::Less, OpCode::Not),
            TokenType::Less => self.emit_opcode(OpCode::Less),
            TokenType::LessEqual => self.emit_opcodes(OpCode::Greater, OpCode::Not),
            TokenType::Plus => self.emit_opcode(OpCode::Add),
            TokenType::Minus => self.emit_opcode(OpCode::Subtract),
            TokenType::Star => self.emit_opcode(OpCode::Multiply),
            TokenType::Slash => self.emit_opcode(OpCode::Divide),
            t => unreachable!("token {:?} that should have been a binary operator", t)
        }
    }

    fn expression(&mut self) {
        self.precedence(Precedence::Assignment);
    }

    fn grouping(&mut self) {
        self.expression();
        self.consume(TokenType::RightParen, "Expected ')' after expression.");
    }

    fn number(&mut self) {
        let val = self.prev.span.parse().unwrap_or(0.);
        if let Err(e) = self.chunk.write_const(Value::Num(val), self.parser.prev.line) {
            self.error(e);
        }
    }

    fn string(&mut self) {
        let val = Rc::new(LoxObject::Str(Box::from(&self.prev.span[1..self.prev.span.len()-1])));
        if let Err(e) = self.chunk.write_const(Value::Obj(val), self.parser.prev.line) {
            self.error(e);
        }
    }

    fn literal(&mut self) {
        let typ = self.prev.typ;
        match typ {
            TokenType::False => self.emit_opcode(OpCode::False),
            TokenType::Nil => self.emit_opcode(OpCode::Nil),
            TokenType::True => self.emit_opcode(OpCode::True),
            t => unreachable!("token {:?} that should have been a literal", t)
        }
    }

    fn unary(&mut self) {
        let op_typ = self.prev.typ;
        self.precedence(Precedence::Unary);
        match op_typ {
            TokenType::Bang => self.emit_opcode(OpCode::Not),
            TokenType::Minus => self.emit_opcode(OpCode::Negate),
            t => unreachable!("token {:?} that should have been a unary operator", t)
        }
    }

    fn precedence(&mut self, prec: Precedence) {
        self.advance();
        let prefix = self.prev.typ.get_rule().prefix;
        if let Some(f) = prefix {
            f(self);
            while prec <= self.current.typ.get_rule().precedence {
                self.advance();
                let infix = self.prev.typ.get_rule().infix;
                let f = infix.expect("infix operator was null");
                f(self);
            }
        } else {
            self.error("Expected expression");
        }
    }

    #[inline(always)]
    fn emit_opcode(&mut self, op: OpCode) {
        let line = self.prev.line;
        self.chunk.push(op.into(), line);
    }

    #[inline(always)]
    #[allow(unused)]
    fn emit_byte(&mut self, b: u8) {
        let line = self.prev.line;
        self.chunk.push(b, line);
    }

    #[inline(always)]
    #[allow(unused)]
    fn emit_two(&mut self, op: OpCode, b: u8) {
        let line = self.prev.line;
        self.chunk.push(op.into(), line);
        self.chunk.push(b, line);
    }

    #[inline(always)]
    fn emit_opcodes(&mut self, fst_op: OpCode, snd_op: OpCode) {
        let line = self.prev.line;
        self.chunk.push(fst_op.into(), line);
        self.chunk.push(snd_op.into(), line);
    }

    #[inline]
    fn emit_return(&mut self) {
        self.emit_opcode(OpCode::Return);
    }
}

#[derive(Clone,Copy,Eq,PartialEq,Ord,PartialOrd)]
enum Precedence {
    None,
    Assignment,
    Or,
    And,
    Eq,
    Cmp,
    Term,
    Factor,
    Unary,
    Call,
    Primary,
}

impl Precedence {
    #[inline]
    fn up(self) -> Self {
        use self::Precedence::*;
        match self {
            None => Assignment,
            Assignment => Or,
            Or => And,
            And => Eq,
            Eq => Cmp,
            Cmp => Term,
            Term => Factor,
            Factor => Unary,
            Unary => Call,
            Call => Primary,
            Primary => Primary,
        }
    }
}

#[derive(Clone, Copy)]
struct ParseRule<'a> {
    prefix: Option<fn(&mut Compiler<'a>)>,
    infix: Option<fn(&mut Compiler<'a>)>,
    precedence: Precedence,
}

impl<'a> ParseRule<'a> {
    fn new(prefix: Option<fn(&mut Compiler<'a>)>,
           infix: Option<fn(&mut Compiler<'a>)>,
           precedence: Precedence) -> Self {
        Self { prefix, infix, precedence }
    }
}

impl<'a> Deref for Compiler<'a> {
    type Target = Parser<'a>;
    fn deref(&self) -> &Self::Target {
        &self.parser
    }
}

impl<'a> DerefMut for Compiler<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.parser
    }
}
