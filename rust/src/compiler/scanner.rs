use std::str::Chars;

use super::{Compiler, ParseRule};

#[derive(Debug)]
pub struct Scanner<'a> {
    pub(crate) source: Chars<'a>,
    pub(crate) start: &'a str,
    pub(crate) offset: usize,
    pub(crate) line: u32,
}

impl<'a> Scanner<'a> {
    pub fn new(source: &'a str) -> Self {
        Scanner {
            source: source.chars(),
            start: source,
            offset: 0,
            line: 1,
        }
    }

    fn make_token(&self, typ: TokenType) -> Token<'a> {
        Token::new(typ, &self.start[..self.offset], self.line)
    }

    fn make_error_token(&self, msg: &'static str) -> Token<'a> {
        Token::new(TokenType::Error, msg, self.line)
    }

    #[inline(always)]
    fn at_end(&self) -> bool {
        self.peek() == None
    }

    #[inline(always)]
    fn next_char(&mut self) -> Option<char> {
        let c = self.source.next();
        self.offset += c.map_or(0, char::len_utf8);
        c
    }

    #[inline(always)]
    fn peek(&self) -> Option<char> {
        self.source.clone().peekable().peek().cloned()
    }

    #[inline(always)]
    fn peek_next(&self) -> Option<char> {
        self.source.clone().nth(1)
    }

    #[inline]
    fn matches(&mut self, c: char) -> bool {
        if self.peek() != Some(c) {
            false
        } else {
            self.next_char();
            true
        }
    }

    fn skip_whitespace(&mut self) -> Option<Token<'a>> {
        loop {
            if let Some(c) = self.peek() {
                match c {
                    ' ' | '\r' | '\t' => { self.next_char(); },
                    '\n' => {
                        self.line += 1;
                        self.next_char();
                    },
                    '/' => match self.peek_next() {
                        Some('/') => while self.peek() != Some('\n') && !self.at_end() {
                            self.next_char();
                        },
                        Some('*') => if let x@Some(_) = self.block_comment() {
                            return x
                        },
                        _ => return None,
                    }
                    _ => return None,
                }
            } else {
                return None;
            }
        }
    }

    fn block_comment(&mut self) -> Option<Token<'a>> {
        let mut clevel = 1;
        while clevel > 0 {
            match self.next_char() {
                Some('\n') => {self.line += 1;},
                Some('/') => if self.matches('*') { clevel += 1; },
                Some('*') => if self.matches('/') { clevel -= 1; },
                Some(_) => {},
                None => break,
            }
        }
        if clevel == 0 {
            None
        } else {
            Some(self.make_error_token("Unexpected end of file"))
        }
    }

    fn string(&mut self) -> Token<'a> {
        while Some('"') != self.peek() && !self.at_end() {
            if self.peek() == Some('\n') {
                self.line += 1;
            }
            self.next_char();
        }

        if self.at_end() {
            self.make_error_token("Unterminated string.")
        } else {
            self.next_char(); // consume final '"'
            self.make_token(TokenType::String)
        }
    }

    fn number(&mut self) -> Token<'a> {
        while self.peek().map_or(false, |c| c.is_ascii_digit()) { self.next_char(); }

        if self.peek() == Some('.') && self.peek_next().map_or(false, |c| c.is_ascii_digit()) {
            self.next_char(); // consume '.'
            while self.peek().map_or(false, |c| c.is_ascii_digit()) { self.next_char(); }
        }
        self.make_token(TokenType::Number)
    }

    fn identifier(&mut self) -> Token<'a> {
        use self::TokenType::*;
        while self.peek().map_or(false, |c| c.is_ascii_alphanumeric()) || self.peek() == Some('_') {
            self.next_char();
        }

        let typ = match &self.start[..self.offset] {
            "and" => And,
            "class" => Class,
            "else" => Else,
            "false" => False,
            "for" => For,
            "fun" => Fun,
            "if" => If,
            "nil" => Nil,
            "or" => Or,
            "print" => Print,
            "return" => Return,
            "super" => Super,
            "this" => This,
            "true" => True,
            "var" => Var,
            "while" => While,
            _ => Identifier,
        };

        self.make_token(typ)
    }
}

macro_rules! tern {
    ($cond:expr, $f:expr, $s:expr) => {
        if $cond { $f } else { $s }
    }
}

macro_rules! tmp {
    ($int:expr, $nm:ident.$fun:ident) => {{
        let tmp = $int;
        $nm.$fun(tmp)
    }}
}

impl<'a> Iterator for Scanner<'a> {
    type Item = Token<'a>;
    fn next(&mut self) -> Option<Self::Item> {
        macro_rules! matches {
            ($ch:expr, $t1:expr, $t2:expr) => {
                tmp!(tern!(self.matches($ch), $t1, $t2), self.make_token)
            }
        }
        use self::TokenType::*;
        match self.skip_whitespace() {
            t@Some(_) => return t, // Unexpected EOF in block comment of whitespace
            None => {},
        }
        self.start = self.source.as_str();
        self.offset = 0;

        if let Some(c) = self.next_char() {
            let tok = match c {
                '(' => self.make_token(LeftParen),
                ')' => self.make_token(RightParen),
                '{' => self.make_token(LeftBrace),
                '}' => self.make_token(RightBrace),
                ',' => self.make_token(Comma),
                '.' => self.make_token(Dot),
                '-' => self.make_token(Minus),
                '+' => self.make_token(Plus),
                ';' => self.make_token(Semicolon),
                '/' => self.make_token(Slash),
                '*' => self.make_token(Star),
                '!' => matches!('=', BangEqual, Bang),
                '=' => matches!('=', EqualEqual, Equal),
                '>' => matches!('=', GreaterEqual, Greater),
                '<' => matches!('=', LessEqual, Less),
                '"' => self.string(),
                'a'..='z' | 'A'..='Z' | '_' =>
                    self.identifier(),
                '0'..='9' => self.number(),
                _ => self.make_error_token("Error, unknown token"),
            };
            Some(tok)
        } else {
            Some(self.make_token(Eof))
        }
    }
}

#[derive(Debug)]
pub struct Token<'a> {
    pub(crate) typ: TokenType,
    pub(crate) span: &'a str,
    pub(crate) line: u32,
}

impl<'a> Token<'a> {
    pub fn new(typ: TokenType, span: &'a str, line: u32) -> Self {
        Self { typ, span, line }
    }
}

#[derive(Clone,Copy,Eq,PartialEq,Debug)]
pub enum TokenType {
    // One Char tokens
    LeftParen, RightParen, LeftBrace, RightBrace,
    Comma, Dot, Minus, Plus,
    Semicolon, Slash, Star,

    // One or Tow char tokens
    Bang, BangEqual, Equal, EqualEqual,
    Greater, GreaterEqual, Less, LessEqual,

    // Literals
    Identifier, String, Number,

    // keywords
    And, Class, Else, False, Fun, For, If, Nil, Or, Print, Return,
    Super, This, True, Var, While,

    Error, Eof
}

impl<'a> TokenType {
    pub(super) fn get_rule(self) -> ParseRule<'a> {
        use self::TokenType::*;
        use super::Precedence as Prec;
        match self {
            LeftParen => ParseRule::new(Some(Compiler::grouping), None, Prec::Call),
            RightParen => ParseRule::new(None, None, Prec::None),
            LeftBrace => ParseRule::new(None, None, Prec::None),
            RightBrace => ParseRule::new(None, None, Prec::None),
            Comma => ParseRule::new(None, None, Prec::None),
            Dot => ParseRule::new(None, None, Prec::Call),
            Minus => ParseRule::new(Some(Compiler::unary), Some(Compiler::binary), Prec::Term),
            Plus => ParseRule::new(None, Some(Compiler::binary), Prec::Term),
            Semicolon => ParseRule::new(None, None, Prec::None),
            Slash => ParseRule::new(None, Some(Compiler::binary), Prec::Factor),
            Star => ParseRule::new(None, Some(Compiler::binary), Prec::Factor),
            Bang => ParseRule::new(Some(Compiler::unary), None, Prec::None),
            BangEqual => ParseRule::new(None, Some(Compiler::binary), Prec::Eq),
            Equal => ParseRule::new(None, None, Prec::None),
            EqualEqual => ParseRule::new(None, Some(Compiler::binary), Prec::Eq),
            Greater => ParseRule::new(None, Some(Compiler::binary), Prec::Cmp),
            GreaterEqual => ParseRule::new(None, Some(Compiler::binary), Prec::Cmp),
            Less => ParseRule::new(None, Some(Compiler::binary), Prec::Cmp),
            LessEqual => ParseRule::new(None, Some(Compiler::binary), Prec::Cmp),
            Identifier => ParseRule::new(None, None, Prec::None),
            String => ParseRule::new(Some(Compiler::string), None, Prec::None),
            Number => ParseRule::new(Some(Compiler::number), None, Prec::None),
            And => ParseRule::new(None, None, Prec::And),
            Class => ParseRule::new(None, None, Prec::None),
            Else => ParseRule::new(None, None, Prec::None),
            False => ParseRule::new(Some(Compiler::literal), None, Prec::None),
            Fun => ParseRule::new(None, None, Prec::None),
            For => ParseRule::new(None, None, Prec::None),
            If => ParseRule::new(None, None, Prec::None),
            Nil => ParseRule::new(Some(Compiler::literal), None, Prec::None),
            Or => ParseRule::new(None, None, Prec::Or),
            Print => ParseRule::new(None, None, Prec::None),
            Return => ParseRule::new(None, None, Prec::None),
            Super => ParseRule::new(None, None, Prec::None),
            This => ParseRule::new(None, None, Prec::None),
            True => ParseRule::new(Some(Compiler::literal), None, Prec::None),
            Var => ParseRule::new(None, None, Prec::None),
            While => ParseRule::new(None, None, Prec::None),
            Error => ParseRule::new(None, None, Prec::None),
            Eof => ParseRule::new(None, None, Prec::None),
        }
    }
}
