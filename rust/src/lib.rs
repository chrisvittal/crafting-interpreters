
use std::fmt;
use chunk::Chunk;

use value::Value;

mod chunk;
mod compiler;
mod object;
mod value;

#[allow(unused)]
macro_rules! op_codes {
    ($($name:ident, $const:ident, $pretty:expr;)*) => {
#[derive(Debug,Clone,Copy,PartialEq,Eq)]
#[repr(u8)]
pub enum OpCode {
    $($name,)*
    Unknown = ::std::u8::MAX,
}

$(pub const $const: u8 = OpCode::$name as u8;)*

impl OpCode {
    fn from_u8(b: u8) -> OpCode {
        match b {
            $($const => OpCode::$name,)*
            _ => OpCode::Unknown,
        }
    }
}

impl Into<u8> for OpCode {
    #[inline(always)]
    fn into(self) -> u8 {
        self as u8
    }
}

impl ::std::fmt::Display for OpCode {
    fn fmt(&self, f: &mut ::std::fmt::Formatter)
        -> ::std::fmt::Result {
        match self {
            $(OpCode::$name => f.pad($pretty),)*
            OpCode::Unknown => f.pad("UNKNOWN"),
        }
    }
}}
}

op_codes!(
    Return, OP_RETURN, "RETURN";
    Constant, OP_CONSTANT, "CONSTANT";
    ConstantLong, OP_CONSTANT_LONG, "CONSTANT_LONG";
    Nil, OP_NIL, "NIL";
    True, OP_TRUE, "TRUE";
    False, OP_FALSE, "FALSE";
    Equal, OP_EQUAL, "EQUAL";
    Greater, OP_GREATER, "GREATER";
    Less, OP_LESS, "LESS";
    Add, OP_ADD, "ADD";
    Subtract, OP_SUBTRACT, "SUBTRACT";
    Multiply, OP_MULTIPLY, "MULTIPLY";
    Divide, OP_DIVIDE, "DIVIDE";
    Not, OP_NOT, "NOT";
    Negate, OP_NEGATE, "NEGATE";
);

#[derive(Debug)]
pub enum InterpreterError {
    Compile,
    Runtime,
}

impl fmt::Display for InterpreterError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            InterpreterError::Compile => f.pad("complier error"),
            InterpreterError::Runtime => f.pad("runtime error"),
        }
    }
}

impl ::std::error::Error for InterpreterError {}

const STACK_MAX: usize = 256;

#[derive(Clone)]
struct Stack {
    sp: usize,
    buf: [Value; STACK_MAX],
}

impl Stack {
    fn push(&mut self, val: Value) {
        self.buf[self.sp] = val;
        self.sp += 1;
    }

    fn pop(&mut self) -> Value {
        let mut val = Value::Nil;
        self.sp -= 1;
        ::std::mem::swap(&mut self.buf[self.sp], &mut val);
        val
    }

    #[allow(unused)]
    fn peek(&self, dist: usize) -> &Value {
        &self.buf[self.sp - 1 - dist]
    }
}

impl fmt::Debug for Stack {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("Stack")
            .field("sp", &self.sp)
            .field("buf", &&self.buf[..self.sp])
            .finish()
    }
}

impl fmt::Display for Stack {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "[ ")?;
        for e in self.buf[..self.sp].iter().rev() {
            write!(f, "{} ", e)?;
        }
        write!(f, "]")
    }
}

#[derive(Debug,Clone)]
pub struct LoxVm {
    chunk: Chunk,
    ip: usize,
    stack: Stack,
}

impl LoxVm {
    pub fn new() -> LoxVm {
        LoxVm {
            chunk: Chunk::new(),
            ip: 0,
            stack: Stack {
                sp: 0,
                buf: nil::ARRAY,
            },
        }
    }

    pub fn interpret(&mut self, source: &str) -> Result<(), InterpreterError> {
        let chunk = compiler::compile(source)?;

        self.chunk = chunk;
        self.ip = 0;
        self.run()
    }

    fn run(&mut self) -> Result<(), InterpreterError> {
        macro_rules! runtime_error {
            ($($arg:tt)*) => {
                eprintln!($($arg)*);
                eprintln!("[line {}] in script", self.chunk.get_line(self.ip))
            }
        }
        macro_rules! read_byte {
            () => {{
                let __ret = self.chunk.code[self.ip];
                self.ip += 1;
                __ret
            }}
        }
        macro_rules! binary_op {
            ($op:tt) => {{
                let b = self.pop();
                let a = self.pop();
                match a $op b {
                    Some(v) => self.push(v),
                    None => {
                        runtime_error!("Operands to '{}' must be numbers", stringify!($op));
                        return Err(InterpreterError::Runtime);
                    }
                }
            }}
        }
        macro_rules! logical_op {
            ($op:tt) => {{
                let b = self.pop();
                let a = self.pop();
                match (a, b) {
                    (Value::Num(a), Value::Num(b)) => self.push(Value::from(a $op b)),
                    _ => {
                        runtime_error!("Operands to '{}' must be numbers", stringify!($op));
                        return Err(InterpreterError::Runtime);
                    }
                }
            }}
        }
        #[cfg(debug_assertions)]
        {
            println!("== run chunk ==")
        }
        loop {
            use OpCode::*;
            #[cfg(debug_assertions)]
            {
                println!("          {}", self.stack);
                self.chunk.disassemble_instruction(self.ip);
            }
            let instruction = OpCode::from_u8(read_byte!());
            match instruction {
                Return => {
                    println!("{}", self.pop());
                    return Ok(())
                },
                Constant => {
                    let v = self.chunk.constants[read_byte!() as usize].clone();
                    self.push(v);
                },
                ConstantLong => {
                    let idx;
                    {
                        let code = &self.chunk.code;
                        idx = code[self.ip] as usize
                            + (code[self.ip+1] as usize) >> 8
                            + (code[self.ip+2] as usize) >> 16;
                    }
                    self.ip += 3;
                    let v = self.chunk.constants[idx].clone();
                    self.push(v);
                },
                Nil => self.push(Value::Nil),
                True => self.push(Value::Bool(true)),
                False => self.push(Value::Bool(false)),
                Equal => {
                    let a = self.pop();
                    let b = self.pop();
                    self.push(Value::Bool(a == b));
                }
                Greater => logical_op!(>),
                Less => logical_op!(<),
                Add => {
                    let b = self.pop();
                    let a = self.pop();
                    match a + b {
                        Some(v) => self.push(v),
                        None => {
                            runtime_error!("Operands to '+' must be two numbers \
                                           or two strings");
                            return Err(InterpreterError::Runtime);
                        }
                    }
                }
                Subtract => binary_op!(-),
                Multiply => binary_op!(*),
                Divide => binary_op!(/),
                Not => {
                    let tmp = self.pop();
                    self.push(!tmp);
                }
                Negate => {
                    let tmp = self.pop();
                    match -tmp {
                        Some(v) => self.push(v),
                        None => {
                            runtime_error!("Operand must be a number");
                            return Err(InterpreterError::Runtime)
                        }
                    }
                }
                Unknown => {},
            }
        }
    }

    #[inline(always)]
    fn push(&mut self, val: Value) {
        self.stack.push(val);
    }

    #[inline(always)]
    fn pop(&mut self) -> Value {
        self.stack.pop()
    }

    #[allow(unused)]
    #[inline(always)]
    fn peek(&self, dist: usize) -> &Value {
        self.stack.peek(dist)
    }
}

mod nil {
    use super::{Value, STACK_MAX};
    use Value::Nil;
    pub const ARRAY: [Value; STACK_MAX] = [
        Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,
        Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,
        Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,
        Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,
        Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,
        Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,
        Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,
        Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,
        Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,
        Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,
        Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,
        Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,
        Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,
        Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,
        Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,
        Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,
    ];
}
