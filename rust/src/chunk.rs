
use OpCode;
use value::Value;

#[derive(Debug, Default, Clone)]
pub struct Chunk {
    pub(crate) code: Vec<u8>,
    pub(crate) constants: Vec<Value>,
    lines: LineArray,
}

impl Chunk {
    pub fn new() -> Self {
        Default::default()
    }

    pub fn push(&mut self, b: u8, line: u32) {
        let line_off: i32 = if self.lines.first_line == 0 {
            self.lines.first_line = line;
            0
        } else {
            line as i32 - self.lines.prev_line as i32
        };
        self.lines.prev_line = line;
        self.lines.push_line(line_off);
        self.code.push(b);
    }

    pub fn write_const(&mut self, val: Value, line: u32) -> Result<(), &'static str> {
        let ind = self.push_const(val);
        if ind > 0xffffff {
            return Err("too many constants in chunk");
        }
        if ind < ::std::u8::MAX as usize {
            self.push(OpCode::Constant as u8, line);
            self.push(ind as u8, line);
        } else {
            self.push(OpCode::ConstantLong as u8, line);
            self.push(ind as u8, line);
            self.push((ind >> 8) as u8, line);
            self.push((ind >> 16) as u8, line);
        }
        Ok(())
    }

    pub fn push_const(&mut self, val: Value) -> usize {
        self.constants.push(val);
        self.constants.len() - 1
    }

    pub fn disassemble(&self, name: &str) {
        println!("== {} ==", name);
        for _ in self.disassembler() { }
    }

    fn disassembler(&self) -> Disassembler {
        Disassembler {
            chunk: self,
            pos: 0,
            prev_line: None,
        }
    }

    #[inline]
    fn print_instruction(&self, opcode: OpCode, offset: usize, prev_line: &mut Option<u32>)
    {
        use OpCode::*;
        print!("  {:04x} ", offset);
        let line = self.get_line(offset);
        if Some(line) == *prev_line {
            print!("   | ");
        } else {
            *prev_line = Some(line);
            print!("{:4} ", line)
        }
        match opcode {
            Return | Nil | True | False | Equal | Greater | Less | Add | Subtract | Multiply
                | Divide | Not | Negate => println!("{}", opcode),
            Constant => {
                let i = self.code[offset + 1];
                let v = &self.constants[i as usize];
                println!("{:16} {} '{}'", opcode, i, v);
            }
            ConstantLong => {
                let i = self.code[offset + 1] as usize
                    + (self.code[offset + 2] as usize >> 8)
                    + (self.code[offset + 3] as usize >> 16);
                let v = &self.constants[i];
                println!("{:16} {} '{}'", opcode, i, v);
            }
            Unknown => println!("Unknown opcode {}", self.code[offset]),
        }
    }

    #[inline]
    fn get_opcode(&self, offset: usize) -> Option<(usize, OpCode)> {
        use OpCode::*;
        let r = match self.code.get(offset).map(|&b| OpCode::from_u8(b)) {
            Some(Return) => (1, Return),
            Some(Constant) => (2, Constant),
            Some(ConstantLong) => (4, ConstantLong),
            Some(Nil) => (1, Nil),
            Some(True) => (1, True),
            Some(False) => (1, False),
            Some(Equal) => (1, Equal),
            Some(Greater) => (1, Greater),
            Some(Less) => (1, Less),
            Some(Add) => (1, Add),
            Some(Subtract) => (1, Subtract),
            Some(Multiply) => (1, Multiply),
            Some(Divide) => (1, Divide),
            Some(Negate) => (1, Negate),
            Some(Not) => (1, Not),
            Some(Unknown) => (1, Unknown),
            None => return None,
        };
        Some(r)
    }

    #[cfg(debug_assertions)]
    pub(crate) fn disassemble_instruction(&self, offset: usize) {
        let (_, opcode) = match self.get_opcode(offset) {
            Some(r) => r,
            None => return,
        };
        self.print_instruction(opcode, offset, &mut None);
    }

    #[cfg(not(debug_assertions))]
    #[allow(unused)]
    pub(crate) fn disassemble_instruction(&self, _: usize) {}

    pub fn get_line(&self, offset: usize) -> u32 {
        let mut addr: usize = 0;
        let mut line = self.lines.first_line as i32;
        for &LineOffset { byte_off, line_off } in self.lines.array.iter() {
            addr += byte_off as usize;
            if addr > offset { break; }
            line = line.wrapping_add(line_off as i32)
        }
        line as u32
    }
}

#[derive(Debug,Clone)]
struct LineArray {
    array: Vec<LineOffset>,
    next_byte_off: usize,
    prev_line: u32,
    first_line: u32,
}

impl LineArray {
    fn push_line(&mut self, mut line_offset: i32) {
        use std::{u8,i8,mem};
        self.next_byte_off = self.next_byte_off.wrapping_add(1);
        if line_offset == 0 {
            return;
        }
        while line_offset != 0 {
            let lo = if self.next_byte_off > u8::MAX as usize {
                self.next_byte_off -= u8::MAX as usize;
                LineOffset::new(u8::MAX, 0)
            } else {
                let btmp = mem::replace(&mut self.next_byte_off, 0) as u8;
                if line_offset < i8::MIN as i32 {
                    line_offset -= i8::MIN as i32;
                    LineOffset::new(btmp, i8::MIN)
                } else if line_offset > i8::MAX as i32 {
                    line_offset -= i8::MAX as i32;
                    LineOffset::new(btmp, i8::MAX)
                } else {
                    let ltmp = mem::replace(&mut line_offset, 0) as i8;
                    LineOffset::new(btmp, ltmp)
                }
            };
            self.array.push(lo)
        }
    }
}

impl Default for LineArray {
    fn default() -> Self {
        Self {
            array: Vec::new(),
            prev_line: 0,
            first_line: 0,
            next_byte_off: ::std::usize::MAX,
        }
    }
}

#[derive(Debug, Clone, Copy)]
struct LineOffset {
    byte_off: u8,
    line_off: i8,
}

impl LineOffset {
    fn new(byte_off: u8, line_off: i8) -> Self {
        Self { byte_off, line_off }
    }
}

#[derive(Debug)]
struct Disassembler<'c> {
    chunk: &'c Chunk,
    pos: usize,
    prev_line: Option<u32>,
}

impl<'c> Iterator for Disassembler<'c> {
    type Item = ();

    fn next(&mut self) -> Option<Self::Item> {
        if let Some((incr, opcode)) = self.chunk.get_opcode(self.pos) {
            self.chunk.print_instruction(opcode, self.pos, &mut self.prev_line);
            self.pos += incr;
            Some(())
        } else {
            None
        }
    }
}

